Instructions to Setup the Project:

1. Clone the code from Bitbucket:  git clone https://aravind_chamakura@bitbucket.org/aravind_chamakura/myretail.git

2. Setup a local single node cassandra instance with all defaults as out of box (This project has been tested on 2.1.8 version of C*)

3. Connect to Cassandra and price.cql file from the resource folder of the project

4. Go into the folder of the code and run mvn install (This will run tests to confirm if application is working.

5. Run the application:  java -jar myRetail-1.0.jar (Server starts on port 20001)

6. Point browser or REST utility (POSTMAN) to any of the following

	1. Read product details with Success 
		http://localhost:20001/products/13860428
	2. Read product with a 404 Error
		http://localhost:20001/products/15117729
	3. Read product with http error 500
		http://localhost:20001/products/16696652
	4. Insert into price database
		PUT  http://localhost:20001/product/price
		
			{
				"listPrice": "69.99",
				"productId": "13860428",
				"offerPrice": "65.99",
				"currencyCode": "USD"
			}	
		

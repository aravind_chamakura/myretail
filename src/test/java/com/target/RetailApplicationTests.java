package com.target;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.AsyncRestTemplate;

import com.target.domain.Product;



@RunWith(SpringRunner.class)
@SpringBootTest //(webEnvironment=WebEnvironment.RANDOM_PORT)
public class RetailApplicationTests {

   private AsyncRestTemplate asyncRestTemplate;
	
    @Before
    public void setup(){
    	asyncRestTemplate = new AsyncRestTemplate();

    }
    
    
   
	@Test
	public void testProductDetailsFound() {
		ListenableFuture<ResponseEntity<Product>> list=  asyncRestTemplate.getForEntity("http://localhost:20001/products/13860428",Product.class);
		try {
			ResponseEntity<Product> response = list.get();
			assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
			assertThat(response.getBody().getName()).isEqualToIgnoringCase("The Big Lebowski (Blu-ray)");
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
	

	@Test
	public void testProductDetailsNotFound() {
		ListenableFuture<ResponseEntity<Product>> list=  asyncRestTemplate.getForEntity("http://localhost:20001/products/15117729",Product.class);
		try {
			if(list.isDone()) {
			ResponseEntity<Product> response = list.get();
			assertThat(response.getStatusCode()).isEqualByComparingTo(HttpStatus.OK);
			}
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
		}
	}
	
}

package com.target.constants;

public class CommonConstants {
	
	public static final String PRODUCT = "product";
	
	public static final String ITEM = "item";
	
	public static final String TCIN = "tcin";
	
	public static final String PRODUCT_DESC = "product_description";

	public static final String TITLE = "title";

	
}

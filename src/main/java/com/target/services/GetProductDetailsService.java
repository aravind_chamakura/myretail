package com.target.services;

import com.target.domain.Product;

import rx.Observable;

public interface GetProductDetailsService {

	public Observable<Product> getProductDetailsForId(String productId);
	

}

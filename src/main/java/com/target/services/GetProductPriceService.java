package com.target.services;

import java.util.Optional;

import com.target.domain.Price;

import rx.Observable;

public interface GetProductPriceService {

	public Observable<Optional<Price>> getPriceByProductId(String productId);
	
}

package com.target.services;

import com.target.domain.Price;

public interface SavePriceService {

	public boolean savePrice(Price price);

}

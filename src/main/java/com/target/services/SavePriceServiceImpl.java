package com.target.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.target.dao.PriceDao;
import com.target.domain.Price;

@Service
public class SavePriceServiceImpl implements SavePriceService {

	@Autowired
	PriceDao priceDao;

	@Override
	public boolean savePrice(Price price) {
		// TODO Auto-generated method stub
		return priceDao.savePriceToDB(price);
	}

}

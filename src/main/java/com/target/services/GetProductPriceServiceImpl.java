package com.target.services;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.target.dao.PriceDao;
import com.target.domain.Price;
import com.target.exceptions.DataAccessException;

import rx.Observable;

@Service
@Qualifier("priceFromDB")
public class GetProductPriceServiceImpl implements GetProductPriceService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetProductPriceServiceImpl.class);

	@Autowired
	PriceDao priceDao;

	@Override
	public Observable<Optional<Price>> getPriceByProductId(String productId) {
		LOGGER.info(String.format("Preparing to get price from Database for product: %s",productId));
		return Observable.<Optional<Price>>create(subscriber -> {
			try {
				subscriber.onNext(Optional.ofNullable(priceDao.getPrice(productId)));
			} catch (Exception e) {
				LOGGER.error(String.format("Error getting price for productId: %s",  productId),e);
				subscriber.onError(new DataAccessException("Exception getting Price"));
			}
			subscriber.onCompleted();
		});
	}

}

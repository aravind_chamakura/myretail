package com.target.services;

import java.util.Optional;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.target.constants.CommonConstants;
import com.target.domain.Price;
import com.target.domain.Product;

import rx.Observable;
import rx.schedulers.Schedulers;

@Service
@Qualifier("productFromRedSky")
public class GetProductDetailsServiceImpl implements GetProductDetailsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetProductDetailsServiceImpl.class);

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	GetProductPriceService productPriceService;

	@Value("${redsky.product.url}")
	private String redSkyProductUrl;

	 @Override
	public Observable<Product> getProductDetailsForId(String productId) {
		Observable<Optional<Product>> o1 = getProductFromRedSky(productId).subscribeOn(Schedulers.io());

		Observable<Optional<Price>> o2 = productPriceService.getPriceByProductId(productId);

		return Observable.zip(o1, o2, (product, price) -> {
			product.ifPresent(response -> {
				response.setPrice(price.get());
			});
			return product.get();

		});

	}

	private Observable<Optional<Product>> getProductFromRedSky(String productId) {
		LOGGER.info(String.format("Preparing to Call Redsky endpoint for product: %s",productId));
		String endPoint = String.format(redSkyProductUrl, productId);
		Function<String, Product> f1 = responseBody -> {
			Product product = new Product();
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(responseBody);
			product.setName(element.getAsJsonObject().getAsJsonObject(CommonConstants.PRODUCT).getAsJsonObject(CommonConstants.ITEM)
					.getAsJsonObject(CommonConstants.PRODUCT_DESC).get(CommonConstants.TITLE).getAsString());
			product.setId(element.getAsJsonObject().getAsJsonObject(CommonConstants.PRODUCT).getAsJsonObject(CommonConstants.ITEM).get(CommonConstants.TCIN)
					.getAsString());
			return product;
		};
		return Observable.<Optional<Product>>create(subscriber -> {
			try {
				ResponseEntity<String> response = restTemplate.getForEntity(endPoint, String.class);
				subscriber.onNext(Optional.ofNullable(f1.apply(response.getBody())));
				subscriber.onCompleted();
			} catch (Exception e) {
				LOGGER.error(String.format("Error getting product info  for product: %s from Redsky API with error: %s",productId, e.getMessage()),e);
				subscriber.onError(e);
			}
		});

	}

}

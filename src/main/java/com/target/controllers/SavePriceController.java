package com.target.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.target.domain.Price;
import com.target.services.SavePriceService;

@RestController
public class SavePriceController {

	@Autowired
	SavePriceService savePrice;
	
	@RequestMapping(value= "/product/price" , method = RequestMethod.PUT , consumes = "application/json")
	public ResponseEntity<?> savePrice(@RequestBody Price price){
		
		if(savePrice.savePrice(price)){
			return ResponseEntity.ok().build();
		}else
			return ResponseEntity.status(500).build();
		 
	}
	
	
}

package com.target.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.async.DeferredResult;

import com.target.domain.Product;
import com.target.services.GetProductDetailsService;

import rx.schedulers.Schedulers;

@RestController
public class GetProductDetailsController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetProductDetailsController.class);

	@Autowired
	private GetProductDetailsService productDetailsService;

	@RequestMapping("/products/{id}")
	public DeferredResult<ResponseEntity<?>> getProduct(@PathVariable("id") String productId){
		LOGGER.info(String.format("Getting product details for product id: %s",productId));
		DeferredResult<ResponseEntity<?>> response = new DeferredResult<>();
		productDetailsService.getProductDetailsForId(productId).subscribeOn(Schedulers.io()).subscribe(
				result ->	{
					LOGGER.info(String.format("Success, returning data for product : %s, %s, ", productId, result));
					response.setResult(ResponseEntity.ok(result));
				},
				error -> { 
					LOGGER.info(String.format("Error getting product details, exception : %s", error.getMessage()));
					if(error instanceof HttpClientErrorException){
						response.setResult( ResponseEntity.status(404).body(new Product()));
					}
					response.setResult( ResponseEntity.status(503).body(response.setErrorResult(error)));
				}
				);
		
		return response;
	}
	
	
}

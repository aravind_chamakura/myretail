package com.target.dao;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.target.domain.Price;
import com.target.util.ClusterConnection;
import com.target.util.MyRetailConfiguration;

@Component
public class PriceDaoImpl implements PriceDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(PriceDaoImpl.class);
	
	@Autowired
	MyRetailConfiguration retailConfiguration;

	private Map<String, PreparedStatement> cachedStatements = new HashMap<String, PreparedStatement>();

	private final String PRICE_READ_STMT = "priceSelect";

	private final String PRICE_INSERT_STMT = "priceInsert";

	private Session session;

	@PostConstruct
	public void createPreparedStatement() {
		try {
		session = ClusterConnection.getInstance().getCluster().connect();
		cachedStatements.put(PRICE_READ_STMT,
				session.prepare(retailConfiguration.getPriceSelect()).setConsistencyLevel(ConsistencyLevel.ONE));
		cachedStatements.put(PRICE_INSERT_STMT,
				session.prepare(retailConfiguration.getInsertPrice()).setConsistencyLevel(ConsistencyLevel.ONE));
		}catch(Exception e){
			LOGGER.error(String.format("Error creating prepared statement with exception: %s", e.getMessage()));
		}
	}

	@Override
	public Price getPrice(String productId) {

		ResultSetFuture resultSetFuture = session.executeAsync(cachedStatements.get(PRICE_READ_STMT).bind(productId));

		Row row = resultSetFuture.getUninterruptibly().one();
		Price price = new Price();
		price.setListPrice(row.getDouble("list_price"));
		price.setOfferPrice(row.getDouble("offer_price"));
		price.setCurrencyCode(row.getString("currency_code"));

		return price;
	}

	@Override
	public boolean savePriceToDB(Price price) {
		boolean success= false;
		
		ResultSetFuture resultSetFuture = session.executeAsync(cachedStatements.get(PRICE_INSERT_STMT)
				.bind(price.getProductId(), price.getListPrice(), price.getOfferPrice(), price.getCurrencyCode()));
		try{
			resultSetFuture.getUninterruptibly();
			success = true;
		}catch(Exception e){
			success=false;
		}

		return success;
	}

}

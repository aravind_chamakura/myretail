package com.target.dao;

import com.target.domain.Price;

public interface PriceDao {

	public Price getPrice(String productId);
	
	public boolean savePriceToDB(Price price);
}

package com.target.domain;

import java.io.Serializable;

public class ProductDetails implements Serializable {

	private static final long serialVersionUID = -8863569691556696102L;

	private Product product;

	private Price price;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}

}

package com.target.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="com.myRetail")
public class MyRetailConfiguration {
	
	private Long queryReadTimeout;
		
	private String defaultKeySpace;
	
	private String insertPrice;
	
	private String priceSelect;
	
	private int port;

	public Long getQueryReadTimeout() {
		return queryReadTimeout;
	}

	public void setQueryReadTimeout(Long queryReadTimeout) {
		this.queryReadTimeout = queryReadTimeout;
	}

	public String getDefaultKeySpace() {
		return defaultKeySpace;
	}

	public void setDefaultKeySpace(String defaultKeySpace) {
		this.defaultKeySpace = defaultKeySpace;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getInsertPrice() {
		return insertPrice;
	}

	public void setInsertPrice(String insertPrice) {
		this.insertPrice = insertPrice;
	}

	public String getPriceSelect() {
		return priceSelect;
	}

	public void setPriceSelect(String priceSelect) {
		this.priceSelect = priceSelect;
	}
	
	
	
	

}

package com.target.util;

import org.springframework.stereotype.Component;

import com.datastax.driver.core.Cluster;

@Component
public class ClusterConnection {

	private volatile static ClusterConnection clusterConnection = null;
	private Cluster cluster = null;
	
	private ClusterConnection(){
		 cluster = Cluster.builder()
		        .withClusterName("Aravind")
		        .addContactPoint("127.0.0.1")
		        .build();
		
	}
	
	public static ClusterConnection getInstance(){
		if(clusterConnection == null){
			synchronized(ClusterConnection.class){
				if(clusterConnection == null)
				clusterConnection = new ClusterConnection();

			}
		}
		
		return clusterConnection;
	}
	
	public Cluster getCluster(){
		return cluster;
	}
	
	
}
